/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.ejercicio3.service;

import cl.ejercicio3.dao.ConstruccionJpaController;
import cl.ejercicio3.dao.exceptions.NonexistentEntityException;
import cl.ejercicio3.entity.Construccion;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author gerva
 */
@Path("construccion")
public class ConstruccionRest {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarConstruccion() {

        ConstruccionJpaController dao = new ConstruccionJpaController();

        List<Construccion> construccion = dao.findConstruccionEntities();

        return Response.ok().entity(construccion).build();
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)

    public Response crear(Construccion construccion) {

        ConstruccionJpaController dao = new ConstruccionJpaController();
        try {
            dao.create(construccion);
        } catch (Exception ex) {
            Logger.getLogger(ConstruccionRest.class.getName()).log(Level.SEVERE, null, ex);
        }

        return Response.ok().entity(construccion).build();
    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response actualizar(Construccion construccion) {

        ConstruccionJpaController dao = new ConstruccionJpaController();
        try {
            dao.edit(construccion);
        } catch (Exception ex) {
            Logger.getLogger(ConstruccionRest.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.ok().entity(construccion).build();
    }

    @DELETE
    @Path("/{ideliminar}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response eliminar(@PathParam("eliminar") String ideliminar) {

        ConstruccionJpaController dao = new ConstruccionJpaController();

        try {
            dao.destroy(ideliminar);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(ConstruccionRest.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.ok().entity("eliminado").build();
    }

    @GET
    @Path("/{idConsultar}")
    @Produces(MediaType.APPLICATION_JSON)

    public Response consultarPorId(@PathParam("consultar") String idConsultar) {

        ConstruccionJpaController dao = new ConstruccionJpaController();

        Construccion construccion = dao.findConstruccion(idConsultar);

        return Response.ok().entity("construccion").build();

    }
}