/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.ejercicio3.dao;

import cl.ejercicio3.dao.exceptions.NonexistentEntityException;
import cl.ejercicio3.dao.exceptions.PreexistingEntityException;
import cl.ejercicio3.entity.Construccion;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author gerva
 */
public class ConstruccionJpaController implements Serializable {

    public ConstruccionJpaController() {
        
    }
    private final EntityManagerFactory emf = Persistence.createEntityManagerFactory("construccionPU");

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Construccion construccion) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(construccion);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findConstruccion(construccion.getNombre()) != null) {
                throw new PreexistingEntityException("Construccion " + construccion + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Construccion construccion) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            construccion = em.merge(construccion);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = construccion.getNombre();
                if (findConstruccion(id) == null) {
                    throw new NonexistentEntityException("The construccion with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Construccion construccion;
            try {
                construccion = em.getReference(Construccion.class, id);
                construccion.getNombre();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The construccion with id " + id + " no longer exists.", enfe);
            }
            em.remove(construccion);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Construccion> findConstruccionEntities() {
        return findConstruccionEntities(true, -1, -1);
    }

    public List<Construccion> findConstruccionEntities(int maxResults, int firstResult) {
        return findConstruccionEntities(false, maxResults, firstResult);
    }

    private List<Construccion> findConstruccionEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Construccion.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Construccion findConstruccion(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Construccion.class, id);
        } finally {
            em.close();
        }
    }

    public int getConstruccionCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Construccion> rt = cq.from(Construccion.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
