/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.ejercicio3.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author gerva
 */
@Entity
@Table(name = "construccion")
@NamedQueries({
    @NamedQuery(name = "Construccion.findAll", query = "SELECT c FROM Construccion c"),
    @NamedQuery(name = "Construccion.findByNombre", query = "SELECT c FROM Construccion c WHERE c.nombre = :nombre"),
    @NamedQuery(name = "Construccion.findByDireccion", query = "SELECT c FROM Construccion c WHERE c.direccion = :direccion"),
    @NamedQuery(name = "Construccion.findByNroPisos", query = "SELECT c FROM Construccion c WHERE c.nroPisos = :nroPisos"),
    @NamedQuery(name = "Construccion.findByPresupuesto", query = "SELECT c FROM Construccion c WHERE c.presupuesto = :presupuesto"),
    @NamedQuery(name = "Construccion.findByInmobiliaria", query = "SELECT c FROM Construccion c WHERE c.inmobiliaria = :inmobiliaria")})
public class Construccion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "nombre")
    private String nombre;
    @Size(max = 2147483647)
    @Column(name = "direccion")
    private String direccion;
    @Size(max = 2147483647)
    @Column(name = "nro_pisos")
    private String nroPisos;
    @Size(max = 2147483647)
    @Column(name = "presupuesto")
    private String presupuesto;
    @Size(max = 2147483647)
    @Column(name = "inmobiliaria")
    private String inmobiliaria;

    public Construccion() {
    }

    public Construccion(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getNroPisos() {
        return nroPisos;
    }

    public void setNroPisos(String nroPisos) {
        this.nroPisos = nroPisos;
    }

    public String getPresupuesto() {
        return presupuesto;
    }

    public void setPresupuesto(String presupuesto) {
        this.presupuesto = presupuesto;
    }

    public String getInmobiliaria() {
        return inmobiliaria;
    }

    public void setInmobiliaria(String inmobiliaria) {
        this.inmobiliaria = inmobiliaria;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (nombre != null ? nombre.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Construccion)) {
            return false;
        }
        Construccion other = (Construccion) object;
        if ((this.nombre == null && other.nombre != null) || (this.nombre != null && !this.nombre.equals(other.nombre))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cl.ejercicio3.entity.Construccion[ nombre=" + nombre + " ]";
    }
    
}
