<%-- 
    Document   : index
    Created on : 4 jul. 2021, 12:01:48
    Author     : gerva
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>URLs</h1>
        <h2> Metodo GET --- https://ejerciciounidad3.herokuapp.com/api/construccion</h2>
        <h2> Metodo POST --- https://ejerciciounidad3.herokuapp.com/api/construccion</h2>
        <h2> Metodo PUT --- https://ejerciciounidad3.herokuapp.com/api/construccion</h2>
        <h2> Metodo DELETE --- https://ejerciciounidad3.herokuapp.com/api/construccion</h2>
        <h2> Metodo GET por Id --- https://ejerciciounidad3.herokuapp.com/api/construccion</h2>
        <h2> Bitbucket --- https://bitbucket.org/Gerardo_Vallejos/pruebaunidad3/src/master/ </h2>
        
        
    </body>
</html>
